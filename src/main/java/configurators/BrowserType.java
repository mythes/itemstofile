package configurators;

public enum BrowserType {
    FIREFOX,
    CHROME
}
