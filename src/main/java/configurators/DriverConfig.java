package configurators;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class DriverConfig {

    private static final String DriverPathMask = "./src/main/resources/webDrivers/";

    private static String getOSName() {
        return System.getProperty("os.name").toLowerCase();
    }

    private static String getPathToWebdriver(String driverFileName) {
        String out;

        if (getOSName().contains("linux")) {
            out = DriverPathMask + "/linux/" + driverFileName.toLowerCase();
        } else if (getOSName().contains("windows")) {
            out = DriverPathMask + "/windows/" + driverFileName.toLowerCase() + ".exe";
        } else {
            System.out.println("Driver not found for your OS. Look at DriverConfig.class");
            out = null;
        }

        return out;
    }

    /**
     * @param browser from BrowserType.enum
     */
    public static WebDriver createDriver(BrowserType browser) throws Exception {

        WebDriver driver = null;

        switch (browser) {
            case CHROME:
                System.out.println("Loading Google Chrome browser");
                System.setProperty("webdriver.chrome.driver", getPathToWebdriver("chromedriver"));
                driver = new ChromeDriver();
                break;
            case FIREFOX:
                System.out.println("Loading Mozilla Firefox browser");
                System.setProperty("webdriver.gecko.driver", getPathToWebdriver("geckodriver"));
                driver = new FirefoxDriver();
                break;

            default:
                throw new WebDriverException("Browser not found!!!! See " + DriverConfig.class.getName());
        }

        driver.manage().window().maximize();

        return driver;
    }
}
