package pageObjects;

import abstractPage.AbstractPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by Alex Alex on 6/20/2017.
 */
public class CategoriesMenu extends AbstractPage {

    @FindBy(css = ".m-cat")
    WebElement categoriesMenu;

    public CategoriesMenu(WebDriver driver) {
        super(driver);
    }

    public CategoriesMenu navigateToTelephone() {
        waitForJSandJQueryToLoad();
        categoriesMenu.findElement(By.xpath(".//*/a[contains (@href, '/telefony/')]")).click();
        return this;
    }

    public ItemsBlock navigateToSmartphones() {
        waitForJSandJQueryToLoad();
        categoriesMenu.findElement(By.xpath(".//div/a[contains (@href, '/preset=smartfon/')]")).click();
        return new ItemsBlock(driver);
    }
}
