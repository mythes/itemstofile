package pageObjects;

import abstractPage.AbstractPage;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import pojo.Product;

import java.util.List;
import java.util.stream.Collectors;

import static java.util.concurrent.TimeUnit.SECONDS;

public class ItemsBlock extends AbstractPage {

    @FindBy(css = "#catalog_goods_block")
    WebElement catalogGoodsBlock;

    public ItemsBlock(WebDriver driver) {
        super(driver);
    }

    private List<WebElement> getItemsFromCatalog() {
        waitForJSandJQueryToLoad();
        return catalogGoodsBlock.findElements(By.cssSelector(".g-i-tile.g-i-tile-catalog .g-i-tile-i-box-desc"));
    }

    public boolean isItemHasTop(WebElement element) {
        waitForJSandJQueryToLoad();
        return element.findElements(By.cssSelector(".g-tag-icon-middle-popularity")).size() > 0;
    }

    private String getNameOfProduct(WebElement catalogItem) {
        waitForJSandJQueryToLoad();
        return catalogItem.findElement(By.cssSelector("div.g-i-tile-i-title.clearfix > a"))
                .getAttribute("innerText");
    }

    private float getPriceOfProduct(WebElement catalogItem) {
        return Float.parseFloat(catalogItem.findElement(By.cssSelector(".g-price-uah"))
                .getText()
                .replaceAll("[^-?0-9]+", ""));
    }

    public List<Product> getTopProducts() {
        return getItemsFromCatalog().stream()
                .filter(this::isItemHasTop)
                .map(element ->
                        new Product(getNameOfProduct(element), getPriceOfProduct(element)))
                .peek(product -> product.setTop(true))
                .collect(Collectors.toList());
    }


}
