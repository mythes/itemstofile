package pageObjects;

import abstractPage.AbstractPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;

import java.time.Duration;


public class MainNavigationMenu extends AbstractPage {

    String urlForSmartphones = "http://rozetka.com.ua/mobile-phones/c80003/filter/preset=smartfon/";

    @FindBy(name = "nav-m-main")
    private WebDriver menu;

    @FindBy(css = "li[id = '3361']")
    private WebElement smartphonesAndElectronicLink;

    public MainNavigationMenu(WebDriver driver) {
        super(driver);
    }

    public CategoriesMenu navigateToSmartphonesAndElectrnics() {
        waitForJSandJQueryToLoad();
        new Actions(driver)
                .moveToElement(smartphonesAndElectronicLink)
                .click()
                .pause(Duration.ofSeconds(3))
                .click()
                .build()
                .perform();
        return new CategoriesMenu(driver);
    }
}
