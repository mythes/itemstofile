package pageObjects;

import abstractPage.AbstractPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class PaginationPage extends AbstractPage{

    @FindBy(name = "paginator")
    WebElement paginator;

    public PaginationPage(WebDriver driver) {
        super(driver);
    }

    public void clickOnPage(int number) throws InterruptedException {
        paginator.findElement(By.id("page" + number)).click();
        waitForJSandJQueryToLoad();
        Thread.sleep(3000);
    }
}
