package pojo;

public class Product {

    private String name;
    private float price;

    boolean isTop = false;
    boolean isNew = false;
    boolean isStock = false;

    public Product(String name, float price){
        this.name = name;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public float getPrice() {
        return price;
    }

    public boolean isTop() {
        return isTop;
    }

    public boolean isNew() {
        return isNew;
    }

    public boolean isStock() {
        return isStock;
    }

    public void setTop(boolean top) {
        isTop = top;
    }

    public void setNew(boolean aNew) {
        isNew = aNew;
    }

    public void setStock(boolean stock) {
        isStock = stock;
    }

    @Override
    public String toString() {
        return "Product{" +
                "name='" + name + '\'' +
                ", price=" + price +
                '}';
    }
}
