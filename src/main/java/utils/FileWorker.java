package utils;

import pojo.Product;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

public class FileWorker {
    public static void write(String fileName, List<? extends Product> list) throws IOException {

        File file = new File("./target/" + fileName);

        try {
            if (!file.exists()) {
                file.createNewFile();
            }

            PrintWriter out = new PrintWriter(file.getAbsoluteFile());

            try {
                for (Object obj : list) {
                    out.println(obj.toString());
                }
            } finally {
                out.close();
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
