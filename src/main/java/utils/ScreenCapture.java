package utils;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import java.io.File;
import java.io.IOException;

public class ScreenCapture {
    private String testClass = null;
    private WebDriver driver = null;
    private int iterator = 1;

    public ScreenCapture(String testClass, WebDriver driver) {
        this.testClass = testClass;
        this.driver = driver;
    }

    public void make() throws IOException {
        File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(scrFile, new File("./target/"
                + System.getProperty("browser")
                + "/"
                + testClass
                + "/screenshot"
                + iterator
                + ".png"));

        iterator++;
    }
}
