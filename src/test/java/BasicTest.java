import configurators.BrowserType;
import configurators.DriverConfig;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import utils.ScreenCapture;


public class BasicTest {

    public WebDriver driver;
    public ScreenCapture screenCapture;

    @BeforeClass(alwaysRun = true)
    @Parameters("browser")
    public void beforeClass(BrowserType browser) throws Exception {
        driver = DriverConfig.createDriver(browser);
        screenCapture = new ScreenCapture(this.getClass().getSimpleName(), driver);
    }

    @AfterClass(alwaysRun = true)
    public void afterClass() throws Exception {
        if (driver != null) {
            driver.close();
            driver.quit();
        }
    }

}
