import org.testng.annotations.Test;
import pageObjects.ItemsBlock;
import pageObjects.MainNavigationMenu;
import pageObjects.PaginationPage;
import pojo.Product;
import utils.FileWorker;

import java.util.ArrayList;
import java.util.List;
@Test(suiteName = "TZ")
public class TopItemTest extends BasicTest {
    @Test(groups = {"funcTests"})
    public void run() throws Exception {
        try {
            MainNavigationMenu mainNavigationMenu = new MainNavigationMenu(driver);
            ItemsBlock itemsBlock = new ItemsBlock(driver);
            PaginationPage paginator = new PaginationPage(driver);

            driver.get("http://rozetka.com.ua/");

            mainNavigationMenu
                    .navigateToSmartphonesAndElectrnics()
                    .navigateToTelephone()
                    .navigateToSmartphones();

//            for(Product product: itemsBlock.getTopProducts()){
//                System.out.println(product.toString());
//            }
            List<Product> result = new ArrayList<>();

            result.addAll(itemsBlock.getTopProducts());
            paginator.clickOnPage(2);
            result.addAll(itemsBlock.getTopProducts());
            paginator.clickOnPage(3);
            result.addAll(itemsBlock.getTopProducts());

            FileWorker.write("output.txt", result);

        } catch (Exception e) {
            screenCapture.make();
            throw (e);
        }
    }
}
